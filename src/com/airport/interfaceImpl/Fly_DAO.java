package com.airport.interfaceImpl;

import com.airport.csv_writer.FlyCompagny_CVS_Writer;
import com.airport.csv_writer.Fly_CSV_Writer;
import com.airport.interfaces.CommonInterface;
import com.airport.model.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Fly_DAO : questa classe implementa quatro methodi per manipolare i dati di un volo.
 * Le operazioni sono ricerca volo, cancellazione volo, aggiunta nuovo volo
 * e ricerca di tutti i voli
 */

public class Fly_DAO implements CommonInterface<Fly_DTO,String> {

    private List<Fly_DTO> fly;

    @Override
    public Fly_DTO get(List<Fly_DTO> t, String s) {
        Fly_DTO fly_dto = new Fly_DTO();
        t.forEach(element->{
            //first I verified if the list contains the searched element and if true I return this element find
            if(element.get_flyNumber().equals(s)){
                fly_dto.set_flyNumber(element.get_flyNumber());
                fly_dto.set_delay(element.get_delay());
                fly_dto.set_from(element.get_from());
                fly_dto.set_to(element.get_to());
                fly_dto.set_gate(element.get_gate());
                fly_dto.set_duration(element.get_duration());
                fly_dto.set_flyArrivalDate(element.get_flyArrivalDate());
                fly_dto.set_flydepartureDate(element.get_flydepartureDate());
                fly_dto.set_flyCompagnyDTO(element.get_flyCompagnyDTO());
            }
        });

      return fly_dto;
    }


    @Override
    public String delete(List<Fly_DTO> t, String s) {
        //First declare a iterator with type Fly_DTO wich allow me to iterate the list and remove a object
        t.forEach(element->{
            if(element.get_flyNumber().equals(s)){
                t.remove(element);
            }
        });
        return "Fligth with number "+s+" was remove succefully";
    }

    @Override
    public String add(Fly_DTO fly_dto) throws IOException {
        Fly_CSV_Writer flycvs_writer = new Fly_CSV_Writer();
        fly =  new ArrayList<>();
        Fly_DTO fly_dto1 = new Fly_DTO();
        AirPort_DTO airPort_from = new AirPort_DTO();
        AirPort_DTO airPort_to = new AirPort_DTO();
        City_DTO city_from = new City_DTO();
        City_DTO city_to = new City_DTO();
        FlyCompagny_DTO flyCompagny_dto = new FlyCompagny_DTO();
        AirPlane_DTO airPlane_dto = new AirPlane_DTO();

        // set new airplane
        airPlane_dto.set_model(fly_dto.get_flyCompagnyDTO().get_airplaneModel().get_model());
        airPlane_dto.set_capacity(fly_dto.get_flyCompagnyDTO().get_airplaneModel().get_capacity());

        // set new compagny
        flyCompagny_dto.set_compagnyName(fly_dto.get_flyCompagnyDTO().get_compagnyName());
        flyCompagny_dto.set_country(fly_dto.get_flyCompagnyDTO().get_country());
        flyCompagny_dto.set_airplaneModel(airPlane_dto);


        //Set a new City From
        city_from.set_cityCode(fly_dto.get_from().get_airportCity().get_cityCode());
        city_from.set_cityCountry(fly_dto.get_from().get_airportCity().get_cityCountry());
        city_from.set_cityName(fly_dto.get_from().get_airportCity().get_cityName());

        // Set a new City To
        city_to.set_cityCode(fly_dto.get_to().get_airportCity().get_cityCode());
        city_to.set_cityCountry(fly_dto.get_to().get_airportCity().get_cityCountry());
        city_to.set_cityName(fly_dto.get_to().get_airportCity().get_cityName());

        // Set a new Airport From
        airPort_from.set_airportName(fly_dto.get_from().get_airportName());
        airPort_from.set_airportCity(city_from);

        // Set a new Airport To
        airPort_to.set_airportName(fly_dto.get_to().get_airportName());
        airPort_to.set_airportCity(city_to);


        // Set a fly_dto1 to add to the return list
        fly_dto1.set_delay(fly_dto.get_delay());
        fly_dto1.set_duration(fly_dto.get_duration());
        fly_dto1.set_flyArrivalDate(fly_dto.get_flyArrivalDate());
        fly_dto1.set_flydepartureDate(fly_dto.get_flydepartureDate());
        fly_dto1.set_flyNumber(fly_dto.get_flyNumber());
        fly_dto1.set_gate(fly_dto.get_gate());
        fly_dto1.set_flyCompagnyDTO(flyCompagny_dto);
        fly_dto1.set_from(airPort_from);
        fly_dto1.set_to(airPort_to);

        // Add new element into our collection list
        fly.add(fly_dto1);
        // Save the new element on csv file
        String output = flycvs_writer.writer_CSV(fly_dto1);
        System.out.println(output);

        return "Fly data add with success!";
    }

    @Override
    public void getAll(List<Fly_DTO> t) {

        //use forEach to iterate and print every element of list of fly
        t.forEach(element->{
            System.out.println(element.get_flyNumber());
            System.out.println(element.get_flyCompagnyDTO().get_compagnyName());
            System.out.println(element.get_from().get_airportCity());
            System.out.println(element.get_to().get_airportCity());
            System.out.println(element.get_flydepartureDate());
            System.out.println(element.get_flyArrivalDate());
            System.out.println(element.get_delay());
            System.out.println(element.get_duration());
            System.out.println(element.get_gate());
            System.out.println("*************************************************");
            });
    }
}
