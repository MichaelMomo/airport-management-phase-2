package com.airport.interfaceImpl;

import com.airport.csv_writer.Airport_CSV_Writer;
import com.airport.interfaces.CommonInterface;
import com.airport.model.AirPort_DTO;
import com.airport.model.City_DTO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Airport_DAO : questa classe implementa quatro methodi per manipolare il l'oggetto AirPort.
 * Le operazioni sono ricerca aeroporto, cancellazione aeroporto, aggiunta nuovo aeroporto e ricerca di tutti gli aeroporti
 */

public class Airport_DAO implements CommonInterface<AirPort_DTO,String> {

    private List<AirPort_DTO> airport;

    @Override
    public AirPort_DTO get(List<AirPort_DTO> t, String s) {

        AirPort_DTO airPort_dto = new AirPort_DTO();
        t.forEach(element->{
            //first I verified if the list contains the searched element and if true I return this element find
            if(element.get_airportName().equals(s)){
                airPort_dto.set_airportCity(element.get_airportCity());
                airPort_dto.set_airportName(element.get_airportName());
            }
        });

        return airPort_dto;
    }

    @Override
    public String delete(List<AirPort_DTO> t, String s) {

        //First I search if the element exit on the list,if it's true I delete a this element
        t.forEach(element->{
            if(element.get_airportName().equals(s)){
                t.remove(element);
            }
        });

        return "Airport   "+s+" was remove succefully";
    }

    @Override
    public String add(AirPort_DTO t) throws IOException {

        airport = new ArrayList<>();
        AirPort_DTO airport1 = new AirPort_DTO();
        City_DTO city = new City_DTO();
        Airport_CSV_Writer airport_csvWriterWriter = new Airport_CSV_Writer();

        //set a new city
        city.set_cityCode(t.get_airportCity().get_cityCode());
        city.set_cityName(t.get_airportCity().get_cityName());
        city.set_cityCountry(t.get_airportCity().get_cityCountry());

        // set new airport
        airport1.set_airportCity(city);
        airport1.set_airportName(t.get_airportName());
        //add a new object airplane in the arraylist
        airport.add(airport1);

        // SAVE THE NEW AIRPORT DATA ON CSV FILE
        String output = airport_csvWriterWriter.writer_CSV(airport1);
        System.out.println(output);

        return "Airport Add with success!";
    }

    @Override
    public void getAll(List<AirPort_DTO> t) {
        //use forEach to iterate and print every element of list of airplane


        t.forEach(element->{
            System.out.println(element.get_airportName());
            System.out.println(element.get_airportCity().get_cityName());
            System.out.println(element.get_airportCity().get_cityCountry());

            System.out.println("*************************************************");
        });
    }
}
