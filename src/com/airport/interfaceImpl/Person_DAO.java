package com.airport.interfaceImpl;

import com.airport.interfaces.CommonInterface;
import com.airport.model.Person_DTO;

import java.util.List;

/**
 * @Person_DAO : questa classe implementa quatro methodi per manipolare i dati una persona.
 * Le operazioni sono ricerca persona, cancellazione persona, aggiunta nuova persona
 * e ricerca di tutte le persone.
 */

public class Person_DAO implements CommonInterface<Person_DTO,String> {
    @Override
    public Person_DTO get(List<Person_DTO> t, String s) {
        return null;
    }

    @Override
    public String delete(List<Person_DTO> t, String s) {
        return null;
    }

    @Override
    public String add(Person_DTO person_dto) {
        return null;
    }

    @Override
    public void getAll(List<Person_DTO> t) {

    }
}
