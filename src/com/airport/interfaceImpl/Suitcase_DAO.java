package com.airport.interfaceImpl;

import com.airport.csv_writer.Suitcase_CVS_Writer;
import com.airport.interfaces.CommonInterface;
import com.airport.model.Suitcase_DTO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Suitcase_DAO : questa classe implementa quatro methodi per manipolare i dati del bagaglio passegero.
 * Le operazioni sono ricerca bagaglio, cancellazione bagaglio, aggiunta nuovo bagaglio
 * e ricerca di tutti i bagagli.
 */

public class Suitcase_DAO implements CommonInterface<Suitcase_DTO,String> {
    private List<Suitcase_DTO> suitcase;
    @Override
    public Suitcase_DTO get(List<Suitcase_DTO> t, String s) {
        Suitcase_DTO suitcase = new Suitcase_DTO();
        t.forEach(element->{
            //first I verified if the list contains the searched element and if true I return this element find
            if(element.get_sCode().equals(s)){
                suitcase.set_sCode(element.get_sCode());
                suitcase.set_sWeight(element.get_sWeight());
            }
        });
        return suitcase;
    }

    @Override
    public String delete(List<Suitcase_DTO> t, String s) {

        //First I search if the element exit on the list,if it's true I delete a this element
        t.forEach(element->{
            if(element.get_sCode().equals(s)){
                t.remove(element);
            }
        });

        return "Suitcase with number "+s+" was remove succefully";
    }

    @Override
    public String add(Suitcase_DTO suitcase_dto) throws IOException {
        suitcase = new ArrayList<>();
        Suitcase_CVS_Writer suitcase_cvs_writer= new Suitcase_CVS_Writer();


        Suitcase_DTO suitcase1 = new Suitcase_DTO();

        // set new suitcase
        suitcase1.set_sWeight(suitcase_dto.get_sWeight());
        suitcase1.set_sCode(suitcase_dto.get_sCode());

        //add a new object suitcase in the arraylist
        suitcase.add(suitcase1);
        // Save the new element on csv file
        String output = suitcase_cvs_writer.writer_CSV(suitcase1);
        System.out.println(output);
        return "Suitcase data add with success!";
    }

    @Override
    public void getAll(List<Suitcase_DTO> t) {

        //use forEach to iterate and print every element of list of suitcase
        t.forEach(element->{
            System.out.println(element.get_sCode());
            System.out.println(element.get_sWeight());
        });
    }
}
