package com.airport.interfaceImpl;

import com.airport.csv_writer.City_CSV_Writer;
import com.airport.interfaces.CommonInterface;
import com.airport.model.City_DTO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @City_DAO : questa classe implementa quatro methodi per manipolare i dati della città.
 * Le operazioni sono ricerca città, cancellazione città, aggiunta nuova città e ricerca di tutte le città.
 */

public class City_DAO implements CommonInterface<City_DTO,String> {
    private List<City_DTO> city;
    @Override
    public City_DTO get(List<City_DTO> t, String s) {
        City_DTO city_dto = new City_DTO();
        t.forEach(element->{
            //first I verified if the list contains the searched element and if true I return this element find
            if(element.get_cityCode().equals(s)){
                city_dto.set_cityCode(element.get_cityCode());
                city_dto.set_cityCountry(element.get_cityCountry());
                city_dto.set_cityName(element.get_cityName());
            }
        });

        return city_dto;
    }

    @Override
    public String delete(List<City_DTO> t, String s) {

        //First I search if the element exit on the list,if it's true I delete a this element
        t.forEach(element->{
            if(element.get_cityCode().equals(s)){
                t.remove(element);
            }
        });

        return "City with number "+s+" was remove succefully";
    }

    @Override
    public String add(City_DTO city_dto) throws IOException {
        City_CSV_Writer city_csvWriter = new City_CSV_Writer();
       city = new ArrayList<>();
       City_DTO city_dto1 = new City_DTO();

       // set new airplane
       city_dto1.set_cityName(city_dto.get_cityName());
       city_dto1.set_cityCountry(city_dto.get_cityCountry());
       city_dto1.set_cityCode(city_dto.get_cityCode());
       //add a new object city in the arraylist
       city.add(city_dto1);
        // Save the new element on csv file
        String output = city_csvWriter.writer_CSV(city_dto1);
        System.out.println(output);
        return "City data add with success!";
    }

    @Override
    public void getAll(List<City_DTO> t) {
        //use forEach to iterate and print every element of list of city
        t.forEach(element->{
            System.out.println(element.get_cityCode());
            System.out.println(element.get_cityName());
            System.out.println(element.get_cityCountry());
        });
    }
}
