package com.airport.interfaceImpl;

import com.airport.csv_writer.Passport_CSV_Writer;
import com.airport.interfaces.CommonInterface;
import com.airport.model.Passport_DTO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Passport_DAO : questa classe implementa quatro methodi per manipolare i dati del passaporto passegero.
 * Le operazioni sono ricerca passaporto, cancellazione passaporto, aggiunta nuovo passaporto
 * e ricerca di tutti i passaporti.
 */

public class Passport_DAO implements CommonInterface<Passport_DTO,String> {
    private List<Passport_DTO> passport;
    @Override
    public Passport_DTO get(List<Passport_DTO> t, String s) {

        Passport_DTO passport_dto = new Passport_DTO();
        t.forEach(element->{
            //first I verified if the list contains the searched element and if true I return this element find
            if(element.get_passportNumber().equals(s)){
                passport_dto.set_passportNumber(element.get_passportNumber());
                passport_dto.set_authority(element.get_authority());
                passport_dto.set_expireDate(element.get_expireDate());
                passport_dto.set_deliverDate(element.get_deliverDate());
            }
        });

        return passport_dto;
    }

    @Override
    public String delete(List<Passport_DTO> t, String s) {

        //First I search if the element exit on the list,if it's true I delete a this element
        t.forEach(element->{
            if(element.get_authority().equals(s)){
                t.remove(element);
            }
        });

        return "Passport with number "+s+" was remove succefully";
    }

    @Override
    public String add(Passport_DTO passport_dto) throws IOException {
        Passport_CSV_Writer passport_csv_writer = new Passport_CSV_Writer();
        passport = new ArrayList<>();
        Passport_DTO passport1 = new Passport_DTO();

        // set new passport
        passport1.set_deliverDate(passport_dto.get_deliverDate());
        passport1.set_expireDate(passport_dto.get_expireDate());
        passport1.set_authority(passport_dto.get_authority());
        passport1.set_passportNumber(passport_dto.get_passportNumber());
        //add a new object passport in the arraylist
        passport.add(passport1);

        // Save the new element on csv file
        String output = passport_csv_writer.writer_CSV(passport1);
        System.out.println(output);
        return "Passport data add with success!";
    }

    @Override
    public void getAll(List<Passport_DTO> t) {
        //use forEach to iterate and print every element of list of passport
        t.forEach(element->{
            System.out.println(element.get_passportNumber());
            System.out.println(element.get_deliverDate());
            System.out.println(element.get_expireDate());
            System.out.println(element.get_authority());

        });

    }
}
