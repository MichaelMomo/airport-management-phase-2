package com.airport.interfaceImpl;

import com.airport.csv_writer.Ticket_CVS_Writer;
import com.airport.interfaces.CommonInterface;
import com.airport.model.TicketChecking_DTO;
import com.airport.model.Ticket_DTO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Ticket_DAO : questa classe implementa quatro methodi per manipolare i dati del biglietto aereo passegero.
 * Le operazioni sono ricerca biglietto aereo, cancellazione biglietto aereo, aggiunta nuovo biglietto aereo
 * e ricerca di tutti i biglietti aereo.
 */

public class Ticket_DAO implements CommonInterface<Ticket_DTO,String> {
    private List<Ticket_DTO> ticket;
    @Override
    public Ticket_DTO get(List<Ticket_DTO> t, String s) {

            Ticket_DTO ticket_dto = new Ticket_DTO();
            t.forEach(element->{
                //first I verified if the list contains the searched element and if true I return this element find
                if(element.get_ticketNumber().equals(s)){
                    ticket_dto.set_ticketNumber(element.get_ticketNumber());
                    ticket_dto.set_airportFrom(element.get_airportFrom());
                    ticket_dto.set_airportTo(element.get_airportTo());
                    ticket_dto.set_arrivalDate(element.get_arrivalDate());
                    ticket_dto.set_departureDate(element.get_departureDate());
                    ticket_dto.set_checkingDate(element.get_checkingDate());
                    ticket_dto.set_flyClass(element.get_flyClass());

                }
            });
            return ticket_dto;
    }

    @Override
    public String delete(List<Ticket_DTO> t, String s) {

        //First I search if the element exit on the list,if it's true I delete a this element
        t.forEach(element->{
            if(element.get_ticketNumber().equals(s)){
                t.remove(element);
            }
        });

        return "Ticket with number "+s+" was remove succefully";
    }

    @Override
    public String add(Ticket_DTO ticket_dto) throws IOException {
        ticket = new ArrayList<>();
        Ticket_CVS_Writer ticket_cvs_writer = new Ticket_CVS_Writer();
        Ticket_DTO ticket1 = new Ticket_DTO();




        // set new ticket
        ticket1.set_ticketNumber(ticket_dto.get_ticketNumber());
        ticket1.set_airportFrom(ticket_dto.get_airportFrom());
        ticket1.set_airportTo(ticket_dto.get_airportTo());
        ticket1.set_departureDate(ticket_dto.get_departureDate());
        ticket1.set_arrivalDate(ticket_dto.get_arrivalDate());
        ticket1.set_flyClass(ticket_dto.get_flyClass());
        ticket1.set_checkingDate(ticket_dto.get_checkingDate());


        //add a new object ticket in the arraylist
        ticket.add(ticket1);
        // Save the new element on csv file
        String output = ticket_cvs_writer.writer_CSV(ticket1);
        System.out.println(output);
        return "Ticket data add with success!";
    }

    @Override
    public void getAll(List<Ticket_DTO> t) {
        //use forEach to iterate and print every element of list of ticket
        t.forEach(element->{
            System.out.println(element.get_ticketNumber());
            System.out.println(element.get_departureDate());
            System.out.println(element.get_arrivalDate());
            System.out.println(element.get_checkingDate());
            System.out.println(element.get_airportFrom());
            System.out.println(element.get_airportTo());
            System.out.println(element.get_flyClass());
            
            System.out.println("*************************************************");
        });

    }
}
