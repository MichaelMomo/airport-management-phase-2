package com.airport.interfaceImpl;

import com.airport.csv_writer.AirPlane_CSV_Writer;
import com.airport.interfaces.CommonInterface;
import com.airport.model.AirPlane_DTO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @AirPlane_DAO : questa classe implementa quatro methodi per manipolare il l'oggetto AirPlane.
 * Le operazioni sono ricerca Aereo, cancellazione Aereo, aggiunta nuovo Aereo e ricerca di tutti gli Aerei
 */

public class AirPlane_DAO implements CommonInterface<AirPlane_DTO,String> {

    private List<AirPlane_DTO>  airplane;

    @Override
    public AirPlane_DTO get(List<AirPlane_DTO> t, String s) {

        AirPlane_DTO airplane_dto = new AirPlane_DTO();
        t.forEach(element->{
            //first I verified if the list contains the searched element and if true I return this element find
            if(element.get_model()==s){
                airplane_dto.set_model(element.get_model());
                airplane_dto.set_capacity(element.get_capacity());
            }
        });

        return airplane_dto;
    }

    @Override
    public String delete(List<AirPlane_DTO> t, String s) {

        //First I search if the element exit on the list,if it's true I delete a this element
        t.forEach(element->{
            if(element.get_model()==s){
                t.remove(element);
            }
        });

        return "Airplane  with model "+s+" was remove succefully";
    }

    @Override
    public String add(AirPlane_DTO airPlane_dto) throws IOException {
        AirPlane_CSV_Writer airplane_csvWriterWriter = new AirPlane_CSV_Writer();
        airplane = new ArrayList<>();
        AirPlane_DTO airplane_dto1  = new AirPlane_DTO();

        // set new airplane
        airplane_dto1.set_capacity(airPlane_dto.get_capacity());
        airplane_dto1.set_model(airPlane_dto.get_model());
        //add a new object airplane in the arraylist
        airplane.add(airplane_dto1);
        // Save the new element on csv file
        String output = airplane_csvWriterWriter.writer_CSV(airplane_dto1);
        System.out.println(output);
        return "Airplane add with success";
    }

    @Override
    public void getAll(List<AirPlane_DTO> t) {
        //use forEach to iterate and print every element of list of airplane
        t.forEach(element->{
            System.out.println(element.get_model());
            System.out.println(element.get_capacity());
        });
    }
}
