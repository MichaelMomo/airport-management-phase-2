package com.airport.exceution;

import com.airport.csv_reader.AirPlane_CSV_Reader;
import com.airport.csv_reader.City_CVS_Reader;
import com.airport.csv_writer.AirPlane_CSV_Writer;
import com.airport.interfaceImpl.*;
import com.airport.model.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.BindException;
import java.util.ArrayList;
import java.util.List;

public class Method_Manager {
    public void methodManager() throws IOException {
        List<AirPlane_DTO> airplanel = new ArrayList<>();
        AirPlane_CSV_Reader csv_reader = new AirPlane_CSV_Reader();

        airplanel= csv_reader.read_CSV();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter a number for an action:");
        String scelta = reader.readLine();
        switch (scelta){
            case "1":
                AirPlane_DTO airplane  = new AirPlane_DTO();
                AirPlane_DAO save_airplane = new AirPlane_DAO();
                // read data from command line
                System.out.println("Enter the model:");
                String _model = reader.readLine();
                System.out.println("Enter the capacity:");
                String _capacity = reader.readLine();
                // set my new airplane object
                airplane.set_model(_model);
                airplane.set_capacity(_capacity);
                // save
                String airplaneList = save_airplane.add(airplane);
                System.out.println(airplaneList);
                break;
            case "2":
                AirPort_DTO airport = new AirPort_DTO();
                Airport_DAO save_airport = new Airport_DAO();
                //read data from command line
                System.out.println("Enter the name of Airport");
                String airportName = reader.readLine();
                // get a list of all the city
                City_CVS_Reader cityReader = new City_CVS_Reader();
                List<City_DTO> dtos;
                dtos =  cityReader.read_CSV();
                System.out.println("City List\n");
                int i = 0;
                dtos.forEach(element->{
                    System.out.println(i+1 + "-"+ element.get_cityName());
                });
                System.out.println("Enter the city name as write");
                String  _city = reader.readLine();
                //search corrisponding element
                dtos.forEach(element->{
                    //if(_city ==  element.get_cityName()){ == don't match
                    if(_city.equals(element.get_cityName())){

                       airport.set_airportName(airportName);
                       airport.set_airportCity(element);
                    }
                });
                // save the new element
                System.out.println(airport);
                String save = save_airport.add(airport);
                System.out.println(save);
                break;
                //*************************************************************************************
            case "3":
                CheckingAgent_DTO checkingAgent_dto = new CheckingAgent_DTO();
                CheckingAgent_DAO save_checkingagent = new CheckingAgent_DAO();
                //Read data from command line
                System.out.println("Enter the name of checking agent");
                String _namechekingagent = reader.readLine();
                System.out.println("Enter the surname of checking agent");
                String _surnamenamechekingagent = reader.readLine();
                System.out.println("Enter the Gender of checking agent");
                String _gendercheckingagent = reader.readLine();
                System.out.println("Enter the address of checking agent");
                String _addresscheckingagent = reader.readLine();
                System.out.println("Enter the date of birth of checking agent");
                String _dateOfBirthcheckingagent = reader.readLine();
                System.out.println("Enter the social number of checking agent");
                String _socialSNumbercheckingagent = reader.readLine();
                System.out.println("Enter the social number of checking agent");
                String _agentNumbercheckingagent =  reader.readLine();
                //set a new checking agent with params enter by a user
                checkingAgent_dto.set_name(_namechekingagent);
                checkingAgent_dto.set_surname(_surnamenamechekingagent);
                checkingAgent_dto.set_gender(_gendercheckingagent);
                checkingAgent_dto.set_address(_addresscheckingagent);
                checkingAgent_dto.set_dateOfBirth(_dateOfBirthcheckingagent);
                checkingAgent_dto.set_socialSNumber(_socialSNumbercheckingagent);
                checkingAgent_dto.set_agentNumber(_agentNumbercheckingagent);
                String savecheckingagent = save_checkingagent.add(checkingAgent_dto);
                System.out.println(savecheckingagent);
                break;
            case "4":
                City_DTO dto = new City_DTO();
                City_DAO dao = new City_DAO();
                // read data from command line
                System.out.println("Enter City name:");
                String _name = reader.readLine();
                System.out.println("Enter Country:");
                String _country = reader.readLine();
                System.out.println("Enter City Code:");
                String _code = reader.readLine();
                // set my new airplane object
                dto.set_cityName(_name);
                dto.set_cityCountry(_country);
                dto.set_cityCode(_code);
                // save
                String saveCity = dao.add(dto);
                System.out.println(saveCity);
                    break;
            case "5":
                FlyCompagny_DTO flyCompagny_dto = new FlyCompagny_DTO();
                Fly_Compagny_DAO save_flycompagny = new Fly_Compagny_DAO();
                System.out.println("Enter the name of fly compagny");
                String _compagnynameflycompagny = reader.readLine();
                System.out.println("Enter the name of country of fly compagny");
                String _compagnycountryflycompagny = reader.readLine();
                //get list of all airplane registred
                AirPlane_CSV_Reader airplaneReader = new AirPlane_CSV_Reader();
                List<AirPlane_DTO> airPlane_dtos;
                airPlane_dtos = airplaneReader.read_CSV();
                System.out.println("Airplane List\n");
                airPlane_dtos.forEach(element->{
                    System.out.println(element.get_model());
                });
                System.out.println("Enter the model of airplane as write");
                String  _modelairplane = reader.readLine();
                //search corrisponding element
                 airPlane_dtos.forEach(element->{
                    if(_modelairplane.equals(element.get_model())){
                        flyCompagny_dto.set_compagnyName(_compagnynameflycompagny);
                        flyCompagny_dto.set_country(_compagnycountryflycompagny);
                        flyCompagny_dto.set_airplaneModel(element);
                    }
                });

                String saveflycompagny = save_flycompagny.add(flyCompagny_dto);
                System.out.println(saveflycompagny);
                break;
            case "7":
                InfoAgent_DTO infoAgent_dto = new InfoAgent_DTO();
                InfoAgent_DAO save_infoagent = new InfoAgent_DAO();
                //Read data from command line
                System.out.println("Enter the name of info agent");
                String _nameinfoagent = reader.readLine();
                System.out.println("Enter the surname of info agent");
                String _surnamenameinfoagent = reader.readLine();
                System.out.println("Enter the Gender of info agent");
                String _genderinfoagent = reader.readLine();
                System.out.println("Enter the address of info agent");
                String _addressinfoagent = reader.readLine();
                System.out.println("Enter the date of birth of info agent");
                String _dateOfBirthinfoagent = reader.readLine();
                System.out.println("Enter the social number of info agent");
                String _socialSNumberinfoagent = reader.readLine();
                System.out.println("Enter the social number of info agent");
                String _agentNumberinfoagent =  reader.readLine();
                //set a new info agent with params enter by a user
                infoAgent_dto.set_name(_nameinfoagent);
                infoAgent_dto.set_surname(_surnamenameinfoagent);
                infoAgent_dto.set_gender(_genderinfoagent);
                infoAgent_dto.set_address(_addressinfoagent);
                infoAgent_dto.set_dateOfBirth(_dateOfBirthinfoagent);
                infoAgent_dto.set_socialSNumber(_socialSNumberinfoagent);
                infoAgent_dto.set_infoAgentNumber(_agentNumberinfoagent);
                String saveinfoagent = save_infoagent.add(infoAgent_dto);
                System.out.println(saveinfoagent);
                break;

            case "9":
                Passport_DTO passport_dto = new Passport_DTO();
                Passport_DAO passport_dao = new Passport_DAO();
                // read data from command line
                System.out.println("Enter Numero of passport:");
                String _passportNumber = reader.readLine();
                System.out.println("Enter deliver date of passport:");
                String _deliverDate = reader.readLine();
                System.out.println("Enter expired date of passport:");
                String _expireDate = reader.readLine();
                System.out.println("Enter deliver authority of passport:");
                String _authority = reader.readLine();
                // set my new passport object
                passport_dto.set_expireDate(_expireDate);
                passport_dto.set_deliverDate(_deliverDate);
                passport_dto.set_passportNumber(_passportNumber);
                passport_dto.set_authority(_authority);

                String savepasseport = passport_dao.add(passport_dto);
                System.out.println(savepasseport);

                break;
            case "10":
                Suitcase_DTO suitcase_dto = new Suitcase_DTO();
                Suitcase_DAO save_suitecase= new Suitcase_DAO();
                // read data from command line
                System.out.println("Enter the code of suitecase:");
                String _codesuitecase = reader.readLine();
                System.out.println("Enter the weight of suitecase:");
                String _weightsuitecase = reader.readLine();
                // set my new suitecase object
                suitcase_dto.set_sCode(_codesuitecase);
                suitcase_dto.set_sWeight(_weightsuitecase);
                // save
                String savesuite = save_suitecase.add(suitcase_dto);
                System.out.println(savesuite);
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + scelta);
        }


    }
}
