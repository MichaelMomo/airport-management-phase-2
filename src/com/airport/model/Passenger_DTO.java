package com.airport.model;

/**
 * @Passenger_DTO
 */

public class Passenger_DTO extends Person_DTO {
    private Suitcase_DTO _passengerSuitcase;
    private Passport_DTO _passengerPassport;
    private Ticket_DTO _passengerTicket;

    public Passenger_DTO() {
        super();
    }

    public Suitcase_DTO get_passengerSuitcase() {
        return _passengerSuitcase;
    }

    public void set_passengerSuitcase(Suitcase_DTO _passengerSuitcase) {
        this._passengerSuitcase = _passengerSuitcase;
    }

    public Passport_DTO get_passengerPassport() {
        return _passengerPassport;
    }

    public void set_passengerPassport(Passport_DTO _passengerPassport) {
        this._passengerPassport = _passengerPassport;
    }

    public Ticket_DTO get_passengerTicket() {
        return _passengerTicket;
    }

    public void set_passengerTicket(Ticket_DTO _passengerTicket) {
        this._passengerTicket = _passengerTicket;
    }
}
