package com.airport.model;

/**
 * @FlyCompagny
 */

public class FlyCompagny_DTO {
    private String _compagnyName;
    private String _country;
    private AirPlane_DTO _airplaneModel;

    public String get_compagnyName() {
        return _compagnyName;
    }

    public void set_compagnyName(String _compagnyName) {
        this._compagnyName = _compagnyName;
    }

    public String get_country() {
        return _country;
    }

    public void set_country(String _country) {
        this._country = _country;
    }

    public AirPlane_DTO get_airplaneModel() {
        return _airplaneModel;
    }

    public void set_airplaneModel(AirPlane_DTO _airplaneModel) {
        this._airplaneModel = _airplaneModel;
    }

    @Override
    public String toString() {
        return "FlyCompagny_DTO{" +
                "_compagnyName='" + _compagnyName + '\'' +
                ", _country='" + _country + '\'' +
                ", _airplaneModel=" + _airplaneModel +
                '}';
    }
}
