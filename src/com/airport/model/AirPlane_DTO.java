package com.airport.model;

/**
 * @AirPlane_DTO
 */

public class AirPlane_DTO {
    private String _model;
    private String _capacity;

    public String get_model() {
        return _model;
    }

    public void set_model(String _model) {
        this._model = _model;
    }

    public String get_capacity() {
        return _capacity;
    }

    public void set_capacity(String _capacity) {
        this._capacity = _capacity;
    }

    @Override
    public String toString() {
        return "AirPlane_DTO{" +
                "_model='" + _model + '\'' +
                ", _capacity='" + _capacity + '\'' +
                '}';
    }
}
