package com.airport.model;

/**
 * @Fly_DTO
 */

public class Fly_DTO {
    private String _flyNumber;
    private String _flydepartureDate;
    private String _flyArrivalDate;
    private String _duration;
    private String _delay;
    private String _gate;
    private AirPort_DTO _from;
    private AirPort_DTO _to;
    private FlyCompagny_DTO _flyCompagnyDTO;

    public Fly_DTO() {
    }

    public String get_flyNumber() {
        return _flyNumber;
    }

    public void set_flyNumber(String _flyNumber) {
        this._flyNumber = _flyNumber;
    }

    public String get_flydepartureDate() {
        return _flydepartureDate;
    }

    public void set_flydepartureDate(String _flydepartureDate) {
        this._flydepartureDate = _flydepartureDate;
    }

    public String get_flyArrivalDate() {
        return _flyArrivalDate;
    }

    public void set_flyArrivalDate(String _flyArrivalDate) {
        this._flyArrivalDate = _flyArrivalDate;
    }

    public String get_duration() {
        return _duration;
    }

    public void set_duration(String _duration) {
        this._duration = _duration;
    }

    public AirPort_DTO get_from() {
        return _from;
    }

    public void set_from(AirPort_DTO _from) {
        this._from = _from;
    }

    public AirPort_DTO get_to() {
        return _to;
    }

    public void set_to(AirPort_DTO _to) {
        this._to = _to;
    }

    public String get_delay() {
        return _delay;
    }

    public void set_delay(String _delay) {
        this._delay = _delay;
    }

    public String get_gate() {
        return _gate;
    }

    public void set_gate(String _gate) {
        this._gate = _gate;
    }

    public FlyCompagny_DTO get_flyCompagnyDTO() {
        return _flyCompagnyDTO;
    }

    public void set_flyCompagnyDTO(FlyCompagny_DTO _flyCompagnyDTO) {
        this._flyCompagnyDTO = _flyCompagnyDTO;
    }

    @Override
    public String toString() {
        return "Fly_DTO{" +
                "_flyNumber='" + _flyNumber + '\'' +
                ", _flydepartureDate='" + _flydepartureDate + '\'' +
                ", _flyArrivalDate='" + _flyArrivalDate + '\'' +
                ", _duration='" + _duration + '\'' +
                ", _from=" + _from +
                ", _to=" + _to +
                ", _delay='" + _delay + '\'' +
                ", _gate='" + _gate + '\'' +
                ", _flyCompagnyDTO=" + _flyCompagnyDTO +
                '}';
    }
}
