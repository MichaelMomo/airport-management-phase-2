package com.airport.model;

/**
 * @TicketChecking
 */

public class TicketChecking_DTO {
    private String _checkingDate;
    private CheckingAgent_DTO _checkingAgent;

    public String get_checkingDate() {
        return _checkingDate;
    }

    public void set_checkingDate(String _checkingDate) {
        this._checkingDate = _checkingDate;
    }

    public CheckingAgent_DTO get_checkingAgent() {
        return _checkingAgent;
    }

    public void set_checkingAgent(CheckingAgent_DTO _checkingAgent) {
        this._checkingAgent = _checkingAgent;
    }

    @Override
    public String toString() {
        return "TicketChecking_DAO{" +
                "_checkingDate='" + _checkingDate + '\'' +
                ", _checkingAgent=" + _checkingAgent +
                '}';
    }
}
