package com.airport.model;

/**
 * @InonfoAgent_DTO
 */

public class InfoAgent_DTO extends Person_DTO {
    private String _infoAgentNumber;

    public InfoAgent_DTO() {
        super();
    }

    public String get_infoAgentNumber() {
        return _infoAgentNumber;
    }

    public void set_infoAgentNumber(String _infoAgentNumber) {
        this._infoAgentNumber = _infoAgentNumber;
    }

    @Override
    public String toString() {
        return "InfoAgent_DTO{" +
                "_infoAgentNumber='" + _infoAgentNumber + '\'' +
                '}';
    }
}
