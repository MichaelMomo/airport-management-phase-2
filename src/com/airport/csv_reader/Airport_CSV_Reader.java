package com.airport.csv_reader;

import com.airport.interfaces.CSV_Reader_Interface;
import com.airport.model.AirPort_DTO;
import com.airport.model.City_DTO;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Airport_CSV_Reader implements CSV_Reader_Interface<AirPort_DTO> {

    private String filename = "AIRPORT.csv";
    private String _delimiter = ";";
    @Override
    public List<AirPort_DTO> read_CSV() {
        List<AirPort_DTO> airportL = new ArrayList<>();
        String line,header;
        try(BufferedReader br = new BufferedReader(new FileReader(filename))){
            if(filename.length()!=0)
                header = br.readLine();
            while ((line = br.readLine())!=null){
                AirPort_DTO airport = new AirPort_DTO();
                City_DTO city_dto = new City_DTO();

                StringTokenizer lineRead = new StringTokenizer(line,_delimiter);
                while(lineRead.hasMoreTokens()){
                    String airportName =  lineRead.nextToken();
                    String  cityName = lineRead.nextToken();
                    String cityCode = lineRead.nextToken();
                    String country= lineRead.nextToken();

                    //set a new city
                    city_dto.set_cityName(cityName);
                    city_dto.set_cityCode(cityCode);
                    city_dto.set_cityCountry(country);
                    //set a new airport
                    airport.set_airportName(airportName);
                    airport.set_airportCity(city_dto);
                    ////////
                    airportL.add(airport);

                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return airportL;
    }
}
