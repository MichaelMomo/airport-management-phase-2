package com.airport.csv_reader;

import com.airport.interfaces.CSV_Reader_Interface;

import com.airport.model.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Ticket_CVS_Reader implements CSV_Reader_Interface<Ticket_DTO> {
    private String filename = "TICKET.csv";
    private String _delimiter = ";";
    @Override
    public List<Ticket_DTO> read_CSV() {
        List<Ticket_DTO> ticketList= new ArrayList<>();
        String line,header;
        try(BufferedReader br = new BufferedReader(new FileReader(filename))){
            if(filename.length()!=0)
                header = br.readLine();
            while ((line = br.readLine())!=null){
                Ticket_DTO ticket_dto = new Ticket_DTO();
                AirPort_DTO _airportFrom = new AirPort_DTO();
                AirPort_DTO _airportTo = new AirPort_DTO();
                City_DTO cityfrom = new City_DTO();
                City_DTO cityto = new City_DTO();

                StringTokenizer lineRead = new StringTokenizer(line,_delimiter);
                while(lineRead.hasMoreTokens()){
                    String _ticketNumber = lineRead.nextToken();
                    String _departureDate = lineRead.nextToken();
                    String _arrivalDate = lineRead.nextToken();
                    String _checkingDate = lineRead.nextToken();
                    String _airportnamefrom = lineRead.nextToken();
                    String _airportcityfrom = lineRead.nextToken();
                    String _airportcountryfrom = lineRead.nextToken();
                    String _airportcitycodefrom = lineRead.nextToken();

                    String _airportnameto = lineRead.nextToken();
                    String _airportcityto = lineRead.nextToken();
                    String _airportcountryto = lineRead.nextToken();
                    String _airportcitycodeto = lineRead.nextToken();
                    String _flyclass = lineRead.nextToken();

                    //set a new city from
                    cityfrom.set_cityName(_airportnamefrom);
                    cityfrom.set_cityCountry(_airportcountryfrom);
                    cityfrom.set_cityCode(_airportcitycodefrom);
                    ////set a new airport from
                    _airportFrom.set_airportName(_airportnamefrom);
                    _airportFrom.set_airportCity(cityfrom);

                    //set a new city to
                    cityto.set_cityName(_airportnameto);
                    cityto.set_cityCountry(_airportcountryto);
                    cityto.set_cityCode(_airportcitycodeto);
                    ////set a new airport to
                    _airportTo.set_airportName(_airportnameto);
                    _airportTo.set_airportCity(cityto);

                    //set a new ticket
                    ticket_dto.set_departureDate(_departureDate);
                    ticket_dto.set_arrivalDate(_arrivalDate);
                    ticket_dto.set_flyClass(_flyclass);
                    ticket_dto.set_checkingDate(_checkingDate);
                    ticket_dto.set_ticketNumber(_ticketNumber);
                    ticket_dto.set_airportTo(_airportTo);
                    ticket_dto.set_airportFrom(_airportFrom);

                    //Add new ticket on the list
                    ticketList.add(ticket_dto);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ticketList;
    }
}
