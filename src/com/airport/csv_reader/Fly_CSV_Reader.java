package com.airport.csv_reader;

import com.airport.interfaces.CSV_Reader_Interface;

import com.airport.model.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Fly_CSV_Reader  implements CSV_Reader_Interface<Fly_DTO>  {
    private String filename = "AIRPORT.csv";
    private String _delimiter = ";";
    @Override
    public List<Fly_DTO> read_CSV() {
        List<Fly_DTO> flylist = new ArrayList<>();
        String line,header;
        try(BufferedReader br = new BufferedReader(new FileReader(filename))){
            if(filename.length()!=0)
                header = br.readLine();
            while ((line = br.readLine())!=null){
                Fly_DTO fly_dto = new Fly_DTO();
                AirPort_DTO airportfrom = new AirPort_DTO();
                AirPort_DTO airportto = new AirPort_DTO();
                City_DTO cityfrom = new City_DTO();
                City_DTO cityto = new City_DTO();
                AirPlane_DTO airPlane_dto = new AirPlane_DTO();
                FlyCompagny_DTO flyCompagny_dto = new FlyCompagny_DTO();

                StringTokenizer lineRead = new StringTokenizer(line,_delimiter);
                while(lineRead.hasMoreTokens()){

                    String _flyNumber =  lineRead.nextToken();
                    String _flydepartureDate =  lineRead.nextToken();
                    String _flyArrivalDate =  lineRead.nextToken();
                    String _duration =  lineRead.nextToken();
                    String _delay =  lineRead.nextToken();
                    String _gate =  lineRead.nextToken();
                    String _airportnamefrom = lineRead.nextToken();
                    String _airportcityfrom = lineRead.nextToken();
                    String _airportcitycodefrom = lineRead.nextToken();
                    String _airportcountryfrom = lineRead.nextToken();
                    String _airportnameto = lineRead.nextToken();
                    String _airportcityto = lineRead.nextToken();
                    String _airportcitycodeto = lineRead.nextToken();
                    String _airportcountryto = lineRead.nextToken();
                    String _compagnyName =  lineRead.nextToken();
                    String  _country = lineRead.nextToken();
                    String airplanemodel = lineRead.nextToken();
                    String airplanecapacity= lineRead.nextToken();

                    //set a new airport from
                    cityfrom.set_cityName(_airportcityfrom);
                    cityfrom.set_cityCode(_airportcitycodefrom);
                    cityfrom.set_cityCountry(_airportcountryfrom);
                    airportfrom.set_airportCity(cityfrom);
                    airportfrom.set_airportName(_airportnamefrom);

                    //set a new airport to
                    cityto.set_cityName(_airportcityto);
                    cityto.set_cityCode(_airportcitycodeto);
                    cityto.set_cityCountry(_airportcountryto);
                    airportto.set_airportCity(cityto);
                    airportto.set_airportName(_airportnameto);
                    //set a new airplane
                    airPlane_dto.set_capacity(airplanecapacity);
                    airPlane_dto.set_model(airplanemodel);
                    //set a new fly compagny
                    flyCompagny_dto.set_country(_country);
                    flyCompagny_dto.set_compagnyName(_compagnyName);

                    //set a new fly
                    fly_dto.set_flyNumber(_flyNumber);
                    fly_dto.set_flydepartureDate(_flydepartureDate);
                    fly_dto.set_flyArrivalDate(_flyArrivalDate);
                    fly_dto.set_duration(_duration);
                    fly_dto.set_delay(_delay);
                    fly_dto.set_gate(_gate);
                    fly_dto.set_from(airportfrom);
                    fly_dto.set_to(airportto);
                    fly_dto.set_flyCompagnyDTO(flyCompagny_dto);
                    ////////
                    flylist.add(fly_dto);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return flylist;
    }
}
