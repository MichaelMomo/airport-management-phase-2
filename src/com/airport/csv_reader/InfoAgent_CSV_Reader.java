package com.airport.csv_reader;

import com.airport.interfaces.CSV_Reader_Interface;
import com.airport.model.InfoAgent_DTO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class InfoAgent_CSV_Reader implements CSV_Reader_Interface<InfoAgent_DTO> {
    private String filename = "INFOAGENT.csv";
    private String _delimiter = ";";
    @Override
    public List<InfoAgent_DTO> read_CSV() {
        List<InfoAgent_DTO> infoagentList= new ArrayList<>();
        String line,header;
        try(BufferedReader br = new BufferedReader(new FileReader(filename))){
            if(filename.length()!=0)
                header = br.readLine();
            while ((line = br.readLine())!=null){
                InfoAgent_DTO infoAgent_dto = new InfoAgent_DTO();

                StringTokenizer lineRead = new StringTokenizer(line,_delimiter);
                while(lineRead.hasMoreTokens()){
                    String name =  lineRead.nextToken();
                    String surname = lineRead.nextToken();
                    String gender = lineRead.nextToken();
                    String address = lineRead.nextToken();
                    String dateOfBirth = lineRead.nextToken();
                    String socialSNumber = lineRead.nextToken();
                    String  infoagentNumber  = lineRead.nextToken();

                    //set a new agent
                    infoAgent_dto.set_name(name);
                    infoAgent_dto.set_surname(surname);
                    infoAgent_dto.set_gender(gender);
                    infoAgent_dto.set_address(address);
                    infoAgent_dto.set_dateOfBirth(dateOfBirth);
                    infoAgent_dto.set_socialSNumber(socialSNumber);
                    infoAgent_dto.set_infoAgentNumber(infoagentNumber);
                    ///
                    infoagentList.add(infoAgent_dto);
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return infoagentList;
    }
}
