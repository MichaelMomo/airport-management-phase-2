package com.airport.csv_reader;

import com.airport.interfaces.CSV_Reader_Interface;
import com.airport.model.AirPlane_DTO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @AirPlane_CSV_Reader this is a function to write a data for a new airplane in a csv file.
 */

public class AirPlane_CSV_Reader implements CSV_Reader_Interface<AirPlane_DTO> {
    private String filname = "AIRPLANES.csv";
    private String _delimiter = ";";
    @Override
    public List<AirPlane_DTO> read_CSV() {
        List<AirPlane_DTO> AirplaneList = new ArrayList<>();
        String line,header;
        try(BufferedReader br = new BufferedReader(new FileReader(filname))){
            /**
             * Read a single line, split inside an Array then set ogni single attributwe of
             * an object Airplane then save a new object in a list that will be return at the end of thuis method
             */
            if(filname.length()!=0)
                header = br.readLine();
            while ((line = br.readLine())!=null){
                AirPlane_DTO airplane = new AirPlane_DTO();
                StringTokenizer lineRead = new StringTokenizer(line,_delimiter);
                while(lineRead.hasMoreTokens()){
                    String _model =  lineRead.nextToken();
                    String  _capacity = lineRead.nextToken();
                    airplane.set_model(_model);
                    airplane.set_capacity(_capacity);
                    AirplaneList.add(airplane);
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return AirplaneList;
    }
}
