package com.airport.csv_reader;

import com.airport.interfaces.CSV_Reader_Interface;
import com.airport.model.AirPort_DTO;
import com.airport.model.CheckingAgent_DTO;
import com.airport.model.City_DTO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class CheckingAgent_CSV_Reader implements CSV_Reader_Interface<CheckingAgent_DTO> {
    private String filename = "CHECKINGAGENT.csv";
    private String _delimiter = ";";
    @Override
    public List<CheckingAgent_DTO> read_CSV() {
        List<CheckingAgent_DTO> checkingagentList= new ArrayList<>();
        String line,header;
        try(BufferedReader br = new BufferedReader(new FileReader(filename))){
            if(filename.length()!=0)
                header = br.readLine();
            while ((line = br.readLine())!=null){
                CheckingAgent_DTO checkingAgent_dto = new CheckingAgent_DTO();

                StringTokenizer lineRead = new StringTokenizer(line,_delimiter);
                while(lineRead.hasMoreTokens()){

                    String name =  lineRead.nextToken();
                    String surname = lineRead.nextToken();
                    String gender = lineRead.nextToken();
                    String address = lineRead.nextToken();
                    String dateOfBirth = lineRead.nextToken();
                    String socialSNumber = lineRead.nextToken();
                    String agentNumber = lineRead.nextToken();
                    //set a new agent
                    checkingAgent_dto.set_name(name);
                    checkingAgent_dto.set_surname(surname);
                    checkingAgent_dto.set_gender(gender);
                    checkingAgent_dto.set_address(address);
                    checkingAgent_dto.set_dateOfBirth(dateOfBirth);
                    checkingAgent_dto.set_socialSNumber(socialSNumber);
                    checkingAgent_dto.set_agentNumber(agentNumber);
                    ///
                    checkingagentList.add(checkingAgent_dto);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return checkingagentList;
    }
}
