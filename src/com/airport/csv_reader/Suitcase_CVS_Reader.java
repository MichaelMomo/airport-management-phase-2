package com.airport.csv_reader;

import com.airport.interfaces.CSV_Reader_Interface;

import com.airport.model.Suitcase_DTO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Suitcase_CVS_Reader implements CSV_Reader_Interface<Suitcase_DTO> {
    private String filname = "SUITECASE.csv";
    private String _delimiter = ";";
    @Override
    public List<Suitcase_DTO> read_CSV() {
        List<Suitcase_DTO> suitcase_dtos = new ArrayList<>();
        String line,header;
        try(BufferedReader br = new BufferedReader(new FileReader(filname))){

            if(filname.length()!=0)
                header = br.readLine();
            while ((line = br.readLine())!=null){
                Suitcase_DTO suitcase_dto = new Suitcase_DTO();
                StringTokenizer lineRead = new StringTokenizer(line,_delimiter);
                while(lineRead.hasMoreTokens()){
                    String _sCode =  lineRead.nextToken();
                    String _sWeight = lineRead.nextToken();
                    suitcase_dto.set_sCode(_sCode);
                    suitcase_dto.set_sWeight(_sWeight);
                    suitcase_dtos.add(suitcase_dto);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return suitcase_dtos;
    }
}
