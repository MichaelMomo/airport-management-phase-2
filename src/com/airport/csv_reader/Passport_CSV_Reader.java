package com.airport.csv_reader;

import com.airport.interfaces.CSV_Reader_Interface;
import com.airport.model.CheckingAgent_DTO;
import com.airport.model.Passport_DTO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Passport_CSV_Reader implements CSV_Reader_Interface<Passport_DTO> {
    private String filename = "PASSPORT.csv";
    private String _delimiter = ";";
    @Override
    public List<Passport_DTO> read_CSV() {
        List<Passport_DTO> passportList= new ArrayList<>();
        String line,header;
        try(BufferedReader br = new BufferedReader(new FileReader(filename))){
            if(filename.length()!=0)
                header = br.readLine();
            while ((line = br.readLine())!=null){
                Passport_DTO passport_dto = new Passport_DTO();

                StringTokenizer lineRead = new StringTokenizer(line,_delimiter);
                while(lineRead.hasMoreTokens()){
                    String _passportNumber =  lineRead.nextToken() ;
                    String _deliverDate =  lineRead.nextToken();
                    String _expireDate =  lineRead.nextToken();
                    String _authority =  lineRead.nextToken();

                    //set a new passport
                    passport_dto.set_passportNumber(_passportNumber);
                    passport_dto.set_deliverDate(_deliverDate);
                    passport_dto.set_expireDate(_expireDate);
                    passport_dto.set_authority(_authority);
                    passportList.add(passport_dto);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return passportList;
    }
}
