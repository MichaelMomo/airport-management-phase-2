package com.airport.csv_reader;

import com.airport.interfaces.CSV_Reader_Interface;
import com.airport.model.AirPlane_DTO;
import com.airport.model.FlyCompagny_DTO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class FlyCompagny_CVS_Reader implements CSV_Reader_Interface<FlyCompagny_DTO> {
    private String filename = "FLYCOMPAGNY.csv";
    private String _delimiter = ";";
    @Override
    public List<FlyCompagny_DTO> read_CSV() {
        List<FlyCompagny_DTO> flycompagnyList = new ArrayList<>();
        String line,header;
        try(BufferedReader br = new BufferedReader(new FileReader(filename))){
            if(filename.length()!=0)
                header = br.readLine();
            while ((line = br.readLine())!=null){

                FlyCompagny_DTO flyCompagny_dto = new FlyCompagny_DTO();
                AirPlane_DTO airPlane_dto = new AirPlane_DTO();

                StringTokenizer lineRead = new StringTokenizer(line,_delimiter);
                while(lineRead.hasMoreTokens()){
                    String _compagnyName =  lineRead.nextToken();
                    String  _country = lineRead.nextToken();
                    String airplanemodel = lineRead.nextToken();
                    String airplanecapacity= lineRead.nextToken();
                    //set a new airplane
                    airPlane_dto.set_model(airplanemodel);
                    airPlane_dto.set_capacity(airplanecapacity);
                    //set a new flycompagnie
                    flyCompagny_dto.set_airplaneModel(airPlane_dto);
                    flyCompagny_dto.set_compagnyName(_compagnyName);
                    flyCompagny_dto.set_country(_country);
                    ////////
                    flycompagnyList.add(flyCompagny_dto);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return flycompagnyList;
    }
}
