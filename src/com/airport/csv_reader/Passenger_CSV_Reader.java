package com.airport.csv_reader;

import com.airport.interfaces.CSV_Reader_Interface;

import com.airport.model.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Passenger_CSV_Reader implements CSV_Reader_Interface<Passenger_DTO> {
    private String filename = "PASSENGER.csv";
    private String _delimiter = ";";
    @Override
    public List<Passenger_DTO> read_CSV() {
        List<Passenger_DTO> passengerList = new ArrayList<>();
        String line,header;
        try(BufferedReader br = new BufferedReader(new FileReader(filename))){
            if(filename.length()!=0)
                header = br.readLine();
            while ((line = br.readLine())!=null){

                Passenger_DTO passenger_dto = new Passenger_DTO();
                Suitcase_DTO suitcase_dto = new Suitcase_DTO();
                Passport_DTO passport_dto = new Passport_DTO();
                AirPort_DTO airportfrom = new AirPort_DTO();
                AirPort_DTO airportto = new AirPort_DTO();
                City_DTO cityfrom = new City_DTO();
                City_DTO cityto = new City_DTO();

                Ticket_DTO ticket_dto = new Ticket_DTO();

                StringTokenizer lineRead = new StringTokenizer(line,_delimiter);
                while(lineRead.hasMoreTokens()){
                    //params of person
                    String _namepassenger = lineRead.nextToken();
                    String _surnamepassenger = lineRead.nextToken();
                    String _gender = lineRead.nextToken();
                    String _address = lineRead.nextToken();
                    String _datebirth = lineRead.nextToken();
                    String _socialnumber = lineRead.nextToken();

                    //params of suitecase
                    String _codesuitecase = lineRead.nextToken();
                    String _weigtsuitecase = lineRead.nextToken();

                    //params of passport
                    String _passportnumber = lineRead.nextToken();
                    String _deliverdate = lineRead.nextToken();
                    String _exipredate = lineRead.nextToken();
                    String _authority = lineRead.nextToken();

                    //params of ticket
                    String _ticketnumber = lineRead.nextToken();
                    String _datedeparture = lineRead.nextToken();
                    String _datearrival = lineRead.nextToken();
                    String _dateChecking = lineRead.nextToken();
                    String _airportnamefrom = lineRead.nextToken();
                    String _airportcityfrom = lineRead.nextToken();
                    String _airportcountryfrom = lineRead.nextToken();
                    String _airportcitycodefrom = lineRead.nextToken();

                    String _airportnameto = lineRead.nextToken();
                    String _airportcityto = lineRead.nextToken();
                    String _airportcountryto = lineRead.nextToken();
                    String _airportcitycodeto = lineRead.nextToken();
                    String _flyclass = lineRead.nextToken();


                    //set a new suitecase
                    suitcase_dto.set_sWeight(_codesuitecase);
                    suitcase_dto.set_sCode(_weigtsuitecase);

                    //set a new passport
                    passport_dto.set_authority(_authority);
                    passport_dto.set_passportNumber(_passportnumber);
                    passport_dto.set_deliverDate(_deliverdate);
                    passport_dto.set_expireDate(_exipredate);


                    //set a new ticket

                    //set a new city from
                    cityfrom.set_cityName(_airportcityfrom);
                    cityfrom.set_cityCode(_airportcitycodefrom);
                    cityfrom.set_cityCountry(_airportcountryfrom);
                    //set a airport from
                    airportfrom.set_airportName(_airportnamefrom);
                    airportfrom.set_airportCity(cityfrom);
                    //set a new city to
                    cityfrom.set_cityName(_airportcityto);
                    cityfrom.set_cityCode(_airportcitycodeto);
                    cityfrom.set_cityCountry(_airportcountryto);
                    //set a airport to
                    airportto.set_airportName(_airportnameto);
                    airportto.set_airportCity(cityto);

                    ticket_dto.set_ticketNumber(_ticketnumber);
                    ticket_dto.set_departureDate(_datedeparture);
                    ticket_dto.set_arrivalDate(_datearrival);
                    ticket_dto.set_checkingDate(_dateChecking);
                    ticket_dto.set_airportFrom(airportfrom);
                    ticket_dto.set_airportTo(airportto);
                    ticket_dto.set_flyClass(_flyclass);
                    //set a new person
                    passenger_dto.set_name(_namepassenger);
                    passenger_dto.set_surname(_surnamepassenger);
                    passenger_dto.set_gender(_gender);
                    passenger_dto.set_address(_address);
                    passenger_dto.set_dateOfBirth(_datebirth);
                    passenger_dto.set_socialSNumber(_socialnumber);
                    passenger_dto.set_passengerSuitcase(suitcase_dto);
                    passenger_dto.set_passengerTicket(ticket_dto);
                    passenger_dto.set_passengerPassport(passport_dto);

                    //Add a new person on a list
                    passengerList.add(passenger_dto);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return passengerList;
    }
}
