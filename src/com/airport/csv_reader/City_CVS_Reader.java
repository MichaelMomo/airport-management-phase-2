package com.airport.csv_reader;

import com.airport.interfaces.CSV_Reader_Interface;
import com.airport.model.AirPlane_DTO;
import com.airport.model.City_DTO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class City_CVS_Reader implements CSV_Reader_Interface<City_DTO> {
    private String filname = "CITY.csv";
    private String _delimiter = ";";
    @Override
    public List<City_DTO> read_CSV() {
        List<City_DTO> _citylist = new ArrayList<>();
        String line,header;
        try(BufferedReader br = new BufferedReader(new FileReader(filname))){
            /**
             * Read a single line, split inside an Array then set ogni single attributwe of
             * an object Airplane then save a new object in a list that will be return at the end of thuis method
             */
            if(filname.length()!=0)
                header = br.readLine();
            while ((line = br.readLine())!=null){
                City_DTO city = new City_DTO();

                StringTokenizer lineRead = new StringTokenizer(line,_delimiter);
                while(lineRead.hasMoreTokens()){
                    String _cityName =  lineRead.nextToken();
                    String _cityCountry = lineRead.nextToken();
                    String _cityCode = lineRead.nextToken();
                    city.set_cityName(_cityName);
                    city.set_cityCountry(_cityCountry);
                    city.set_cityCode(_cityCode);
                    _citylist.add(city);

                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return _citylist;
    }
}
