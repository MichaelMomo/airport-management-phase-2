package com.airport.csv_writer;

import com.airport.interfaces.CSV_Writer_Interface;
import com.airport.model.AirPort_DTO;
import com.airport.model.FlyCompagny_DTO;
import com.airport.model.Fly_DTO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Fly_CSV_Writer implements CSV_Writer_Interface<String, Fly_DTO> {
    private String output;
    private String file_name = "FLY";
    private final static String msg = "Fly Saved Correctly in CSV File";
    @Override
    public String writer_CSV(Fly_DTO fly_dto) throws IOException {

        output = file_name + ".csv";
        File file = new File(output);
        FileWriter csvWriter = new FileWriter(file,true);
        if (file.length()==0) {

            csvWriter.append("FLY NUMBER;");
            csvWriter.append("FLY DEPARTURE DATE;");
            csvWriter.append("FLY ARRIVAL DATE;");
            csvWriter.append("DURATION OF FLY;");
            csvWriter.append("DELAY OF FLY;");
            csvWriter.append("GATE OF FLY;");
            csvWriter.append("Airport Name FROM;");
            csvWriter.append("CITY Name FROM;");
            csvWriter.append("POSTAL CODE FROM;");
            csvWriter.append("COUNTRY FROM;");
            csvWriter.append("Airport Name TO;");
            csvWriter.append("CITY Name TO;");
            csvWriter.append("POSTAL CODE TO;");
            csvWriter.append("COUNTRY TO;");
            csvWriter.append("Compagny Name;");
            csvWriter.append("Name of country;");
            csvWriter.append("Model of airplane;");
            csvWriter.append("Capacity of airplane;");
            csvWriter.append("\n");
        }

        csvWriter.append(fly_dto.get_flyNumber());
        csvWriter.append(fly_dto.get_flydepartureDate());
        csvWriter.append(fly_dto.get_flyArrivalDate());
        csvWriter.append(fly_dto.get_duration());
        csvWriter.append(fly_dto.get_delay());
        csvWriter.append(fly_dto.get_gate());
        csvWriter.append(fly_dto.get_from().get_airportName());
        csvWriter.append(fly_dto.get_from().get_airportCity().get_cityName());
        csvWriter.append(fly_dto.get_from().get_airportCity().get_cityCode());
        csvWriter.append(fly_dto.get_from().get_airportCity().get_cityCountry());
        csvWriter.append(fly_dto.get_to().get_airportName());
        csvWriter.append(fly_dto.get_to().get_airportCity().get_cityName());
        csvWriter.append(fly_dto.get_to().get_airportCity().get_cityCode());
        csvWriter.append(fly_dto.get_to().get_airportCity().get_cityCountry());
        csvWriter.append(fly_dto.get_flyCompagnyDTO().get_compagnyName());
        csvWriter.append(fly_dto.get_flyCompagnyDTO().get_country());
        csvWriter.append(fly_dto.get_flyCompagnyDTO().get_airplaneModel().get_model());
        csvWriter.append(fly_dto.get_flyCompagnyDTO().get_airplaneModel().get_capacity());
        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();
        return msg;
    }
}
