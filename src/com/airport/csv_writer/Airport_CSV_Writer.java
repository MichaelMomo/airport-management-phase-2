package com.airport.csv_writer;

import com.airport.csv_reader.City_CVS_Reader;
import com.airport.interfaceImpl.City_DAO;
import com.airport.interfaces.CSV_Writer_Interface;
import com.airport.model.AirPort_DTO;
import com.airport.model.City_DTO;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Airport_CSV_Writer implements CSV_Writer_Interface<String, AirPort_DTO> {

    private String output;
    private String file_name = "AIRPORT";
    private final static String msg = "Airport Saved Correctly in CSV File";

    @Override
    public String writer_CSV(AirPort_DTO airPort_dto) throws IOException {

        output = file_name + ".csv";
        File file = new File(output);
        FileWriter csvWriter = new FileWriter(file,true);

        if (file.length()==0) {
            csvWriter.append("Airport Name;");
            csvWriter.append("CITY Name;");
            csvWriter.append("POSTAL CODE;");
            csvWriter.append("COUNTRY");
            csvWriter.append("\n");
        }
        csvWriter.append(airPort_dto.get_airportName());
        csvWriter.append(";");
        csvWriter.append(airPort_dto.get_airportCity().get_cityName());
        csvWriter.append(";");
        csvWriter.append(airPort_dto.get_airportCity().get_cityCode());
        csvWriter.append(";");
        csvWriter.append(airPort_dto.get_airportCity().get_cityCountry());
        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();
        return msg;
    }

}
