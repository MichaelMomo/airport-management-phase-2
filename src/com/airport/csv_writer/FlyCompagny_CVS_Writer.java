package com.airport.csv_writer;

import com.airport.interfaces.CSV_Writer_Interface;
import com.airport.model.AirPlane_DTO;
import com.airport.model.FlyCompagny_DTO;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FlyCompagny_CVS_Writer implements CSV_Writer_Interface<String, FlyCompagny_DTO> {
    private String output;
    private String filename = "FLYCOMPAGNY";
    private final static String msg = "Fly Compagny saved Correctly in CSV File";
    @Override
    public String writer_CSV(FlyCompagny_DTO flyCompagny_dto) throws IOException {
        output = filename + ".csv";
        File file = new File(output);
        FileWriter csvWriter = new FileWriter(file,true);

        if (file.length()==0) {

            csvWriter.append("Compagny Name;");
            csvWriter.append("Name of country;");
            csvWriter.append("Model of airplane;");
            csvWriter.append("Capacity of airplane");
            csvWriter.append("\n");
        }
        csvWriter.append(flyCompagny_dto.get_compagnyName());
        csvWriter.append(";");
        csvWriter.append(flyCompagny_dto.get_country());
        csvWriter.append(";");
        csvWriter.append(flyCompagny_dto.get_airplaneModel().get_model());
        csvWriter.append(";");
        csvWriter.append(flyCompagny_dto.get_airplaneModel().get_capacity());
        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();
        return msg;
    }
}
