package com.airport.csv_writer;

import com.airport.interfaces.CSV_Writer_Interface;
import com.airport.model.Suitcase_DTO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Suitcase_CVS_Writer implements CSV_Writer_Interface<String, Suitcase_DTO> {
    private String output;
    private final static String msg = "SuiteCase Saved Correctly in CSV File";
    private String filname = "SUITCASE";

    @Override
    public String writer_CSV(Suitcase_DTO suitcase_dto) throws IOException {
        output = filname + ".csv";
        File file = new File(output);
        FileWriter csvWriter = new FileWriter(file,true);
        if (file.length()==0) {
            csvWriter.append("Suite case Code");
            csvWriter.append(";");
            csvWriter.append("WEIGHT");
            csvWriter.append("\n");
        }
        csvWriter.append(suitcase_dto.get_sCode());
        csvWriter.append(";");
        csvWriter.append(suitcase_dto.get_sWeight());
        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();
        return msg;
    }
}
