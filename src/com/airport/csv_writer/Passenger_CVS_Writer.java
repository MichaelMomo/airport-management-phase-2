package com.airport.csv_writer;

import com.airport.interfaces.CSV_Writer_Interface;
import com.airport.model.InfoAgent_DTO;
import com.airport.model.Passenger_DTO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Passenger_CVS_Writer implements CSV_Writer_Interface<String, Passenger_DTO> {
    private String output;
    private String file_name = "PASSENGER";
    private final static String msg = "Passenger Saved Correctly in CSV File";

    @Override
    public String writer_CSV(Passenger_DTO passenger_dto) throws IOException {
        output = file_name + ".csv";
        File file = new File(output);
        FileWriter csvWriter = new FileWriter(file,true);

        if (file.length()==0) {
            csvWriter.append("Name OF PASSENGER;");
            csvWriter.append("SURNAME OF PASSENGER;");
            csvWriter.append("GENDER;");
            csvWriter.append("ADDRESS;");
            csvWriter.append("DATE OF BIRTH;");
            csvWriter.append("SOCIAL NUMBER;");
            //PARAMS TO SUITECASE
            csvWriter.append("CODE OF SUITECASE;");
            csvWriter.append("WEIGHT OF SUITECASE;");
            //PARAMS TO PASSPORT
            csvWriter.append("PASSPORT NUMBER;");
            csvWriter.append("DELIVER DATE;");
            csvWriter.append("EXPIRATE DATE;");
            csvWriter.append("AUTHORITY;");
            //PARAMS TO TICKET
            csvWriter.append("TICKET NUMBER;");
            csvWriter.append("DATE DEPARTURE;");
            csvWriter.append("DATE ARRIVAL;");
            csvWriter.append("CHECKING DATE PROGRAMMED;");

            csvWriter.append("AIRPORT NAME FROM;");
            csvWriter.append("CITY NAME OF AIRPORT FROM;");
            csvWriter.append("COUNTRY OF AIRPORT FROM;");
            csvWriter.append("CITY CODE OF AIRPORT FROM;");

            csvWriter.append("AIRPORT NAME TO;");
            csvWriter.append("CITY NAME OF AIRPORT TO;");
            csvWriter.append("COUNTRY OF AIRPORT TO;");
            csvWriter.append("CITY CODE OF AIRPORT FROM;");
            csvWriter.append("FLY CLASS");
            csvWriter.append("\n");
        }


        csvWriter.append(passenger_dto.get_name());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_surname());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_gender());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_address());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_dateOfBirth());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_socialSNumber());
        csvWriter.append(";");
        //new suitecase of passenger
        csvWriter.append(passenger_dto.get_passengerSuitcase().get_sCode());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerSuitcase().get_sWeight());
        csvWriter.append(";");
        //new passport of passenger
        csvWriter.append(passenger_dto.get_passengerPassport().get_passportNumber());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerPassport().get_deliverDate());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerPassport().get_expireDate());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerPassport().get_authority());
        csvWriter.append(";");
        //new ticket of passenger

        csvWriter.append(passenger_dto.get_passengerTicket().get_ticketNumber());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerTicket().get_departureDate());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerTicket().get_arrivalDate());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerTicket().get_checkingDate());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerTicket().get_airportFrom().get_airportName());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerTicket().get_airportFrom().get_airportCity().get_cityName());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerTicket().get_airportFrom().get_airportCity().get_cityCountry());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerTicket().get_airportFrom().get_airportCity().get_cityCode());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerTicket().get_airportTo().get_airportName());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerTicket().get_airportTo().get_airportCity().get_cityName());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerTicket().get_airportTo().get_airportCity().get_cityCountry());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerTicket().get_airportTo().get_airportCity().get_cityCode());
        csvWriter.append(";");
        csvWriter.append(passenger_dto.get_passengerTicket().get_flyClass());
        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();
        return msg;
    }
}
