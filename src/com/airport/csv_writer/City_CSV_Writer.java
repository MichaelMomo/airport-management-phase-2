package com.airport.csv_writer;

import com.airport.interfaces.CSV_Writer_Interface;
import com.airport.model.City_DTO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @City_CSV_Writer
 */

public class City_CSV_Writer implements CSV_Writer_Interface<String, City_DTO> {
    private String output;
    private final static String msg = "City Saved Correctly in CSV File";
    private String filname = "CITY";

    @Override
    public String writer_CSV(City_DTO city_dto) throws IOException {
        output = filname + ".csv";
        File file = new File(output);
        FileWriter csvWriter = new FileWriter(file,true);
        if (file.length()==0) {
            csvWriter.append("NAME");
            csvWriter.append(";");
            csvWriter.append("COUNTRY");
            csvWriter.append(";");
            csvWriter.append("CITY CODE");
            csvWriter.append("\n");
        }
        csvWriter.append(city_dto.get_cityName());
        csvWriter.append(";");
        csvWriter.append(city_dto.get_cityCountry());
        csvWriter.append(";");
        csvWriter.append(city_dto.get_cityCode());
        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();
        return msg;
    }
}
