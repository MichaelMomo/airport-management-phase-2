package com.airport.csv_writer;

import com.airport.interfaces.CSV_Writer_Interface;

import com.airport.model.Ticket_DTO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Ticket_CVS_Writer implements CSV_Writer_Interface<String, Ticket_DTO> {
    private String output;
    private String file_name = "TICKET";
    private final static String msg = "Ticket Saved Correctly in CSV File";


    @Override
    public String writer_CSV(Ticket_DTO ticket_dto) throws IOException {
        output = file_name + ".csv";
        File file = new File(output);
        FileWriter csvWriter = new FileWriter(file,true);

        if (file.length()==0) {
            csvWriter.append("TICKET NUMBER;");
            csvWriter.append("DATE DEPARTURE;");
            csvWriter.append("DATE ARRIVAL;");
            csvWriter.append("CHECKING DATE PROGRAMMED;");

            csvWriter.append("AIRPORT NAME FROM;");
            csvWriter.append("CITY NAME OF AIRPORT FROM;");
            csvWriter.append("COUNTRY OF AIRPORT FROM;");
            csvWriter.append("CITY CODE OF AIRPORT FROM;");

            csvWriter.append("AIRPORT NAME TO;");
            csvWriter.append("CITY NAME OF AIRPORT TO;");
            csvWriter.append("COUNTRY OF AIRPORT TO;");
            csvWriter.append("CITY CODE OF AIRPORT TO;");
            csvWriter.append("FLY CLASS");
            csvWriter.append("\n");
        }
        csvWriter.append(ticket_dto.get_ticketNumber());
        csvWriter.append(";");
        csvWriter.append(ticket_dto.get_departureDate());
        csvWriter.append(";");
        csvWriter.append(ticket_dto.get_arrivalDate());
        csvWriter.append(";");
        csvWriter.append(ticket_dto.get_checkingDate());
        csvWriter.append(";");
        csvWriter.append(ticket_dto.get_airportFrom().get_airportName());
        csvWriter.append(";");
        csvWriter.append(ticket_dto.get_airportFrom().get_airportCity().get_cityName());
        csvWriter.append(";");
        csvWriter.append(ticket_dto.get_airportFrom().get_airportCity().get_cityCountry());
        csvWriter.append(";");
        csvWriter.append(ticket_dto.get_airportFrom().get_airportCity().get_cityCode());
        csvWriter.append(";");
        csvWriter.append(ticket_dto.get_airportTo().get_airportName());
        csvWriter.append(";");
        csvWriter.append(ticket_dto.get_airportTo().get_airportCity().get_cityName());
        csvWriter.append(";");
        csvWriter.append(ticket_dto.get_airportTo().get_airportCity().get_cityCountry());
        csvWriter.append(";");
        csvWriter.append(ticket_dto.get_airportTo().get_airportCity().get_cityCode());
        csvWriter.append(";");
        csvWriter.append(ticket_dto.get_flyClass());

        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();
        return msg;
    }
}
