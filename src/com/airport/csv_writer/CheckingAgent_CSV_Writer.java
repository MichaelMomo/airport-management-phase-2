package com.airport.csv_writer;

import com.airport.interfaces.CSV_Writer_Interface;
import com.airport.model.CheckingAgent_DTO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class CheckingAgent_CSV_Writer implements CSV_Writer_Interface<String, CheckingAgent_DTO> {
    private String output;
    private String filename = "CHECKINGAGENT";
    private final static String msg = "Checking Agent saved Correctly in CSV File";

    @Override
    public String writer_CSV(CheckingAgent_DTO checkingAgent_dto) throws IOException {
        output = filename + ".csv";
        File file = new File(output);
        FileWriter csvWriter = new FileWriter(file);
        if (file.length()==0) {
            csvWriter.append("Name of Person;");
            csvWriter.append("Surname of Person;");
            csvWriter.append("Gender;");
            csvWriter.append("Address");
            csvWriter.append("Date of Birth;");
            csvWriter.append("Social Number;");
            csvWriter.append("Agent Number");
            csvWriter.append("\n");
        }
        csvWriter.append(checkingAgent_dto.get_name());
        csvWriter.append(";");
        csvWriter.append(checkingAgent_dto.get_surname());
        csvWriter.append(";");
        csvWriter.append(checkingAgent_dto.get_gender());
        csvWriter.append(";");
        csvWriter.append(checkingAgent_dto.get_address());
        csvWriter.append(";");
        csvWriter.append(checkingAgent_dto.get_dateOfBirth());
        csvWriter.append(";");
        csvWriter.append(checkingAgent_dto.get_socialSNumber());
        csvWriter.append(";");
        csvWriter.append(checkingAgent_dto.get_agentNumber());
        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();

        return msg;

    }
}
