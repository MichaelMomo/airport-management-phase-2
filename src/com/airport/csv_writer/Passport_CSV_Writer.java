package com.airport.csv_writer;

import com.airport.interfaces.CSV_Writer_Interface;

import com.airport.model.Passport_DTO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Passport_CSV_Writer implements CSV_Writer_Interface<String, Passport_DTO> {
    private String output;
    private final static String msg = "Passport Saved Correctly in CSV File";
    private String filname = "PASSPORT";
    @Override
    public String writer_CSV(Passport_DTO passport_dto) throws IOException {
        output = filname + ".csv";
        File file = new File(output);
        FileWriter csvWriter = new FileWriter(file,true);

        if (file.length()==0) {
            csvWriter.append("PASSPORT NUMBER;");
            csvWriter.append("DELIVER DATE;");
            csvWriter.append("EXPIRATE DATE;");
            csvWriter.append("AUTHORITY;");
            csvWriter.append("\n");
        }
        /**
         * then add a new data for a new airplane
         */
        csvWriter.append(passport_dto.get_passportNumber());
        csvWriter.append(";");
        csvWriter.append(passport_dto.get_deliverDate());
        csvWriter.append(";");
        csvWriter.append(passport_dto.get_expireDate());
        csvWriter.append(";");
        csvWriter.append(passport_dto.get_authority());
        csvWriter.append(";");
        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();
        return msg;
    }
}
