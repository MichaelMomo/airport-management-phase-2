package com.airport.csv_writer;

import com.airport.interfaces.CSV_Writer_Interface;
import com.airport.model.AirPlane_DTO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @AirPlane_CSV_Writer this class is use to write a new airplane data into a  csv file.it have an method calling
 * writer_CSV thatr takes in input an object of type Airplane_DTO  an return a string(exit message)
 */

public class AirPlane_CSV_Writer implements CSV_Writer_Interface<String,AirPlane_DTO> {
    private String output;
    private final static String msg = "Airplane Saved Correctly in CSV File";
    private String filname = "AIRPLANES";
    @Override
    public String writer_CSV(AirPlane_DTO airPlane_dto) throws IOException {
        output = filname + ".csv";
        File file = new File(output);
        FileWriter csvWriter = new FileWriter(file,true);
        /**
         * if the file don't exist, create a new file then add a header
         */

        if (file.length()==0) {
            csvWriter.append("MODEL");
            csvWriter.append(";");
            csvWriter.append("CAPACITY");
            csvWriter.append("\n");
        }
        /**
         * then add a new data for a new airplane
         */
        csvWriter.append(airPlane_dto.get_model());
        csvWriter.append(";");
        csvWriter.append(airPlane_dto.get_capacity());
        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();
        return msg;
    }
}
