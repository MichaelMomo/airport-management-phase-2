package com.airport.csv_writer;

import com.airport.interfaces.CSV_Writer_Interface;
import com.airport.model.InfoAgent_DTO;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class InfoAgent_CSV_Writer implements CSV_Writer_Interface<String, InfoAgent_DTO> {
    private String output;
    private String filename = "INFOAGENT";
    private final static String msg = "Info Agent saved Correctly in CSV File";

    @Override
    public String writer_CSV(InfoAgent_DTO infoAgent_dto) throws IOException {
        System.out.println("sono in infowtiter:"+infoAgent_dto);
        output = filename + ".csv";
        File file = new File(output);
        FileWriter csvWriter = new FileWriter(file);
        if (file.length()==0) {
            csvWriter.append("Name of Person;");
            csvWriter.append("Surname of Person;");
            csvWriter.append("Gender;");
            csvWriter.append("Address;");
            csvWriter.append("Date of Birth;");
            csvWriter.append("Social Number;");
            csvWriter.append("Info Agent Number");
            csvWriter.append("\n");
        }
        csvWriter.append(infoAgent_dto.get_name());
        csvWriter.append(";");
        csvWriter.append(infoAgent_dto.get_surname());
        csvWriter.append(";");
        csvWriter.append(infoAgent_dto.get_gender());
        csvWriter.append(";");
        csvWriter.append(infoAgent_dto.get_address());
        csvWriter.append(";");
        csvWriter.append(infoAgent_dto.get_dateOfBirth());
        csvWriter.append(";");
        csvWriter.append(infoAgent_dto.get_socialSNumber());
        csvWriter.append(";");
        csvWriter.append(infoAgent_dto.get_infoAgentNumber());
        csvWriter.append("\n");
        csvWriter.flush();
        csvWriter.close();

        return msg;
    }
}
