package com.airport.interfaces;

import java.io.IOException;

public interface CSV_Writer_Interface<S,T> {
    public S writer_CSV(T t) throws IOException;
}
