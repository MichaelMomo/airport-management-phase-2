package com.airport.interfaces;

import java.util.List;

public interface CSV_Reader_Interface<S> {

    public List<S> read_CSV();
}
